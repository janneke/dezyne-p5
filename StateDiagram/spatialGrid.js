/*
 * Copyright (C) 2023 Rob Wieringa <rma.wieringa@gmail.com>
 *
 * This file is part of Dezyne-P5.
 * Dezyne-P5 offers Dezyne web views based on p5.js
 *
 * Dezyne-P5 is free software, it is distributed under the terms of
 * the GNU General Public Licence version 3 or later.
 * See <http://www.gnu.org/licenses/>.
 */

class SpatialGrid {
  // see e.g. https://en.wikipedia.org/wiki/Grid_(spatial_index)
  constructor(minpos, maxpos, nrx, nry) {
    this.bounds = {x: minpos.x, y: minpos.y, width: maxpos.x-minpos.x, height: maxpos.y-minpos.y};
    this.nrx = nrx;
    this.nry = nry;
    // each cell is an array of elements;
    this.cells = [...Array(nrx)].map(row => [...Array(nry)].map(c => []));
    this.nrElements = 0;
  }

  newElement(node) {
    let elt = { node: node, px: node.pos.x, py: node.pos.y, index: -1 };
    this.insertElement(elt);
    node.element = elt;
    return elt;
  }

  insertElement(elt) {
    let ixy = this.getCellIndex(elt.px, elt.py);
    let cell = this.cells[ixy.x][ixy.y];
    cell.push(elt);
    elt.index = cell.length-1;
    this.nrElements++;
  }

  getCellIndex(px, py) {
    let fx = (px-this.bounds.x) / this.bounds.width;
    let ix = Math.floor(fx * (this.nrx-1));
    let fy = (py-this.bounds.y) / this.bounds.height;
    let iy = Math.floor(fy * (this.nry-1));
    return {x: ix, y: iy};
  }

  nodesInScope(px, py, radius) {
    let result = [];
    let i1xy = this.getCellIndex(Math.max(px-radius, this.bounds.x),
                                 Math.max(py-radius, this.bounds.y));
    let i2xy = this.getCellIndex(Math.min(px+radius, this.bounds.x+this.bounds.width),
                                 Math.min(py+radius, this.bounds.y+this.bounds.height));
    for (let ix = i1xy.x; ix <= i2xy.x; ix++) {
      for (let iy = i1xy.y; iy <= i2xy.y; iy++) {
        let cell = this.cells[ix][iy];
        for (let elt of cell) {
          if ((px-elt.px)*(px-elt.px) + ((py-elt.py)*(py-elt.py)) < radius*radius)
            result.push(elt.node);
        }
      }
    }
    return result;
  }

  updateElement(elt) {
    // naive
    this.removeElement(elt);
    this.insertElement(elt);
  }

  removeElement(elt) {
    let index = elt.index;
    let ixy = this.getCellIndex(elt.px, elt.py);
    let cell = this.cells[ixy.x][ixy.y];
    cell[index] = cell[cell.length-1];
    cell[index].index = index;
    cell.pop();
    this.nrElements--;
  }
}
